FROM debian:10-slim
ARG PROJECTNAME=torvus
WORKDIR /opt/app
COPY ./torvus /opt/app/torvus
ENV RUST_BACKTRACE=full
CMD ["./torvus"]