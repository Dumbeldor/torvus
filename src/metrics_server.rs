use prometheus::{Encoder, Registry, TextEncoder};

use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Response, Server};
use std::{error::Error, net::SocketAddr};

pub struct ServerMetrics {
    registry: Option<Registry>,
}

impl ServerMetrics {
    #[allow(clippy::clippy::new_without_default)] // TODO: Check if it's usefull
    pub fn new() -> Self {
        let registry = Some(Registry::new());

        Self { registry }
    }

    pub fn registry(&self) -> &Registry {
        match self.registry {
            Some(ref r) => r,
            None => panic!("You cannot longer register new metrics after the server has started!"),
        }
    }

    pub fn run(&mut self, addr: SocketAddr) -> Result<(), Box<dyn Error>> {
        let registry = self
            .registry
            .take()
            .expect("ServerMetrics must be already started");

        tokio::spawn(async move {
            log::debug!("starting hyper server to serve metrics");

            let service = make_service_fn(move |_| {
                let registry_clone = registry.clone();

                async move {
                    Ok::<_, hyper::Error>(service_fn(move |_req| {
                        let mf = registry_clone.gather();
                        let encoder = TextEncoder::new();
                        let mut buffer = vec![];

                        encoder
                            .encode(&mf, &mut buffer)
                            .expect("Failed to encoder metrics text.");

                        async move { Ok::<_, hyper::Error>(Response::new(Body::from(buffer))) }
                    }))
                }
            });

            let server = Server::bind(&addr).serve(service);

            let graceful = server.with_graceful_shutdown(shutdown_signal());

            if let Err(e) = graceful.await {
                // Idk if it's a good idea to panic here?
                // if we assume that Torvus shouldnt work without metric, why not
                panic!("Metrics error: {}", e);
            }
            log::debug!("stopping hyper server to serve metrics");
        });
        Ok(())
    }
}

async fn shutdown_signal() {
    // Wait for the CTRL+C Signal
    tokio::signal::ctrl_c()
        .await
        .expect("Failed to install CTRL+C signal handler");
}
